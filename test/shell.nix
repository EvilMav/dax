{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
, daxpkgs ? import <dax-full> { inherit pkgs; }
, daxVersion ? null
}:

let
  dax = pkgs.python3Packages.callPackage ../derivation.nix { inherit daxVersion; inherit (artiqpkgs) artiq sipyco; inherit (daxpkgs) trap-dac-utils; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      # Packages required for testing
      ps.pytest
      ps.mypy
      ps.pycodestyle
      ps.coverage
      daxpkgs.flake8-artiq
      daxpkgs.artiq-stubs
    ] ++ dax.propagatedBuildInputs))
    # Required for compile testcases
    artiqpkgs.binutils-or1k
    artiqpkgs.llvm-or1k
  ];
}
