import logging
import numpy as np

from artiq import __version__ as _artiq_version
from dax import __version__ as _dax_version
from dax.experiment import *
import dax.util.random

import test.hw_test

_NUM_ITERATIONS = 10000
_SEED = np.int64(0x4d595df4d0f33173)


def _suppress_warnings():
    sup = np.testing.suppress_warnings()
    sup.filter(RuntimeWarning, "overflow encountered in .*")
    return sup


class _RandomTestSystem(DaxSystem):
    SYS_ID = 'unittest_system'
    SYS_VER = 0
    CORE_LOG_KEY = None
    DAX_INFLUX_DB_KEY = None

    def build(self, *, rng_class, **kwargs):
        super().build()
        self.rnd = rng_class(self.core, **kwargs)
        self.kernel_invariants.add("rnd")
        self.result = []

    def init(self) -> None:
        self.logger.info(f'ARTIQ {_artiq_version}')
        self.logger.info(f'DAX {_dax_version}')

    @rpc(flags={"async"})
    def _save(self, v):
        self.result.append(v)

    @kernel
    def randint32(self):
        self.rnd.init(_SEED)
        for _ in range(_NUM_ITERATIONS):
            self._save(self.rnd.randint32())

    @kernel
    def randint64(self):
        self.rnd.init(_SEED)
        for _ in range(_NUM_ITERATIONS):
            self._save(self.rnd.randint64())

    @kernel
    def randrange(self, start, stop):
        self.rnd.init(_SEED)
        for _ in range(_NUM_ITERATIONS):
            self._save(self.rnd.randrange(start, stop))

    @kernel
    def randfloat(self):
        self.rnd.init(_SEED)
        for _ in range(_NUM_ITERATIONS):
            self._save(self.rnd.randfloat())

    @kernel
    def benchmark(self, iterations: TInt32) -> TTuple([TInt64, TInt32]):  # type: ignore[valid-type]
        self.rnd.init(_SEED)
        acc = np.int32(0)
        t_start = self.core.get_rtio_counter_mu()
        for _ in range(iterations):
            acc ^= self.rnd.randint32()
        t_end = self.core.get_rtio_counter_mu()
        return t_end - t_start, acc


class RandomTestCase(test.hw_test.HardwareTestCase):

    def setUp(self) -> None:
        # Call super
        super().setUp()
        # Construct environment
        self.env = self.construct_env(_RandomTestSystem, build_kwargs=dict(rng_class=dax.util.random.PCG_XSH_RR))

    def test_randint32(self):
        # Run kernel
        self.env.randint32()

        with _suppress_warnings():
            # Create reference and compare if the result is equal
            self.env.rnd.init(_SEED)
            ref = [self.env.rnd.randint32() for _ in range(_NUM_ITERATIONS)]
            self.assertListEqual(self.env.result, ref)

    def test_randint64(self):
        # Run kernel
        self.env.randint64()

        with _suppress_warnings():
            # Create reference and compare if the result is equal
            self.env.rnd.init(_SEED)
            ref = [self.env.rnd.randint64() for _ in range(_NUM_ITERATIONS)]
            self.assertListEqual(self.env.result, ref)

    def test_randrange(self):
        for start, stop in [(0, 100), (0, 1), (-20, 20), (-1000, -1), (-2**31, 2**31 - 1)]:
            # Due to subtests, we have to clear the results
            self.env.result = []
            # Run kernel
            self.env.randrange(start, stop)

            with _suppress_warnings():
                # Create reference and compare if the result is equal
                self.env.rnd.init(_SEED)
                ref = [self.env.rnd.randrange(start, stop) for _ in range(_NUM_ITERATIONS)]
                self.assertListEqual(self.env.result, ref)

    def test_randfloat(self):
        # Run kernel
        self.env.randfloat()

        with _suppress_warnings():
            # Create reference and compare if the result is equal
            self.env.rnd.init(_SEED)
            ref = [self.env.rnd.randfloat() for _ in range(_NUM_ITERATIONS)]
            self.assertListEqual(self.env.result, ref)

    def test_performance(self, iterations=100000):
        # Set logging level
        self.env.logger.setLevel(logging.INFO)
        # Run benchmark
        t, _ = self.env.benchmark(iterations)
        # Report result
        class_name = self.env.rnd.__class__.__name__
        self.env.logger.info(f"{class_name} total time: {t} ns")
        self.env.logger.info(f"{class_name} iterations: {iterations}")
        self.env.logger.info(f"{class_name} time per iteration: {t / iterations} ns")
