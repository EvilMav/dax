import random

from artiq.experiment import *
from artiq.coredevice.ad9910 import (
    RAM_DEST_FTW, RAM_DEST_ASF, RAM_DEST_POW, RAM_DEST_POWASF,
    PHASE_MODE_TRACKING,
)

import dax.sim.test_case
import dax.sim.coredevice.urukul
import dax.sim.coredevice.ad9910
from dax.util.artiq_version import ARTIQ_MAJOR_VERSION

if ARTIQ_MAJOR_VERSION >= 7:
    from artiq.coredevice.urukul import DEFAULT_PROFILE
else:
    DEFAULT_PROFILE = 0

import test.sim.coredevice._compile_testcase as compile_testcase
from test.environment import CI_ENABLED

_NUM_SAMPLES = 1000 if CI_ENABLED else 100

_DEVICE_DB = {
    'core': {
        'type': 'local',
        'module': 'artiq.coredevice.core',
        'class': 'Core',
        'arguments': {'host': None, 'ref_period': 1e-9}
    },
    "dut": {
        "type": "local",
        "module": "artiq.coredevice.ad9910",
        "class": "AD9910",
        "arguments": {
            "pll_en": 0,
            "chip_select": 6,
            "cpld_device": "cpld",
        }
    },
    "dut1": {
        "type": "local",
        "module": "artiq.coredevice.ad9910",
        "class": "AD9910",
        "arguments": {
            "chip_select": 4,
            "cpld_device": "cpld",
            "pll_en": 0,
            "io_update_delay": 3
        }
    },
    "dut2": {
        "type": "local",
        "module": "artiq.coredevice.ad9910",
        "class": "AD9910",
        "arguments": {
            "chip_select": 4,
            "cpld_device": "cpld",
            "pll_en": 0,
            "sync_delay_seed": "eeprom_urukul0:00",
            "io_update_delay": "eeprom_urukul0:00"
        }
    },
    "cpld": {
        "type": "local",
        "module": "artiq.coredevice.urukul",
        "class": "CPLD",
        "arguments": {
            "spi_device": "spi_urukul0",
            "refclk": 1e9,
            "clk_div": 1
        }
    },
    'spi_urukul0': {
        'type': 'local',
        'module': 'artiq.coredevice.spi2',
        'class': 'SPIMaster',
    },
}


class _Environment(HasEnvironment):
    def build(self):
        self.core = self.get_device('core')
        self.dut = self.get_device('dut')
        self.dut1 = self.get_device('dut1')
        self.dut2 = self.get_device('dut2')


class AD9910TestCase(dax.sim.test_case.PeekTestCase):
    SEED = None

    def setUp(self) -> None:
        self.rng = random.Random(self.SEED)
        self.env = self.construct_env(_Environment, device_db=_DEVICE_DB)

    def _test_uninitialized(self):
        self.expect(self.env.dut, 'init', 'x')
        self.expect(self.env.dut, 'freq', 'x')
        self.expect(self.env.dut, 'phase', 'x')
        self.expect(self.env.dut, 'phase_mode', 'x')
        self.expect(self.env.dut, 'amp', 'x')
        self.expect(self.env.dut, 'ram_enable', 'x')
        self.expect(self.env.dut, 'ram_dest', 'x')
        self.expect(self.env.dut, 'internal_profile', 'x')

        self.assertEqual(self.env.dut.sync_data.sync_delay_seed, -1)
        self.assertEqual(self.env.dut.sync_data.io_update_delay, 0)
        self.assertEqual(self.env.dut1.sync_data.sync_delay_seed, -1)
        self.assertEqual(self.env.dut1.sync_data.io_update_delay, 3)
        self.assertEqual(self.env.dut2.sync_data.sync_delay_seed, 0)
        self.assertEqual(self.env.dut2.sync_data.io_update_delay, 0)

    def test_init(self):
        self._test_uninitialized()
        self.env.dut.init()
        self.expect(self.env.dut, 'init', 1)
        self.expect(self.env.dut, 'ram_enable', 0)
        self.expect(self.env.dut, 'ram_dest', '00')
        self.expect(self.env.dut, 'internal_profile', 0)
        self.assertEqual(self.env.dut.sync_data.sync_delay_seed, -1)
        self.assertEqual(self.env.dut.sync_data.io_update_delay, 0)

    def test_init_sync_data(self):
        self._test_uninitialized()
        self.env.dut1.init()
        self.env.dut2.init()
        for d in self.env.dut1, self.env.dut2:
            self.expect(d, 'init', 1)
            self.expect(d, 'ram_enable', 0)
            self.expect(d, 'ram_dest', '00')
            self.expect(self.env.dut, 'internal_profile', 0)

        self.assertEqual(self.env.dut1.sync_data.sync_delay_seed, -1)
        self.assertEqual(self.env.dut1.sync_data.io_update_delay, 3)
        self.assertEqual(self.env.dut2.sync_data.sync_delay_seed, -1)
        self.assertEqual(self.env.dut2.sync_data.io_update_delay, 0)

    def test_power_down(self):
        self._test_uninitialized()
        for bits in range(1 << 4):
            self.env.dut.power_down(bits)
            self.expect(self.env.dut, 'power_down', f'{bits:04b}')

    def test_phase_mode_timing(self):
        self._test_uninitialized()
        self.env.dut.set_phase_mode(dax.sim.coredevice.ad9910.PHASE_MODE_ABSOLUTE)
        self._test_uninitialized()
        self.env.dut.set(100 * MHz)
        self.expect(self.env.dut, 'phase_mode', '01')

    def test_default_phase_mode_timing(self):
        self._test_uninitialized()
        self.env.dut.set_mu(2 ** 30)
        self.expect(self.env.dut, 'phase_mode', '00')

    def test_write64(self):
        self._test_uninitialized()
        prev_profile = DEFAULT_PROFILE
        for _ in range(_NUM_SAMPLES):
            f = self.rng.uniform(0 * MHz, 400 * MHz)
            p = self.rng.randrange(2 ** 16)
            a = self.rng.randrange(2 ** 14)
            profile = self.rng.randrange(8)
            self.env.dut.write64(0x0E + profile, (a << 16) | (p & 0xffff), self.env.dut.frequency_to_ftw(f))
            if prev_profile == profile:
                self.env.dut.cpld.io_update.pulse_mu(8)
            else:
                self.env.dut.cpld.set_profile(profile)
                prev_profile = profile
            self.expect_close(self.env.dut, 'freq', f, places=0)
            self.expect(self.env.dut, 'phase', self.env.dut.pow_to_turns(p))
            self.expect(self.env.dut, 'amp', self.env.dut.asf_to_amplitude(a))

    def test_set_mu(self):
        self._test_uninitialized()
        for _ in range(_NUM_SAMPLES):
            f = self.rng.uniform(0 * MHz, 400 * MHz)
            p = self.rng.randrange(2 ** 16)
            a = self.rng.randrange(2 ** 14)
            self.env.dut.set_mu(self.env.dut.frequency_to_ftw(f), pow_=p, asf=a)
            self.expect_close(self.env.dut, 'freq', f, places=0)
            self.expect(self.env.dut, 'phase', self.env.dut.pow_to_turns(p))
            self.expect(self.env.dut, 'phase_mode', '00')
            self.expect(self.env.dut, 'amp', self.env.dut.asf_to_amplitude(a))
            self.expect(self.env.dut, 'ram_enable', 0)
            self.expect(self.env.dut, 'ram_dest', '00')
            self.expect(self.env.dut, 'internal_profile', 0)

    def test_set_mu_tracking(self):
        self._test_uninitialized()
        self.env.dut.set_phase_mode(PHASE_MODE_TRACKING)
        for _ in range(_NUM_SAMPLES):
            ref_time_mu = now_mu()
            delay_mu(1000000)
            f = self.rng.uniform(0 * MHz, 400 * MHz)
            p = self.rng.randrange(2 ** 16)
            a = self.rng.randrange(2 ** 14)
            pow_ = self.env.dut.set_mu(self.env.dut.frequency_to_ftw(f), pow_=p, asf=a, ref_time_mu=ref_time_mu)
            self.expect_close(self.env.dut, 'freq', f, places=0)
            self.expect_close(self.env.dut, 'phase', self.env.dut.pow_to_turns(pow_) % 1.0, places=4)
            self.expect(self.env.dut, 'phase_mode', dax.sim.coredevice.ad9910._PHASE_MODE_DICT[PHASE_MODE_TRACKING])
            self.expect(self.env.dut, 'amp', self.env.dut.asf_to_amplitude(a))
            self.expect(self.env.dut, 'ram_enable', 0)
            self.expect(self.env.dut, 'ram_dest', '00')
            self.expect(self.env.dut, 'internal_profile', 0)

    def test_set(self):
        self._test_uninitialized()
        for _ in range(_NUM_SAMPLES):
            f = self.rng.uniform(0 * MHz, 400 * MHz)
            p = self.rng.uniform(0.0, self.env.dut.pow_to_turns(0xFFFF))
            a = self.rng.uniform(0.0, 1.0)
            self.env.dut.set(f, phase=p, amplitude=a)
            self.expect_close(self.env.dut, 'freq', f, places=0)
            self.expect_close(self.env.dut, 'phase', p, places=4)
            self.expect(self.env.dut, 'phase_mode', '00')
            self.expect_close(self.env.dut, 'amp', a, places=4)
            self.expect(self.env.dut, 'ram_enable', 0)
            self.expect(self.env.dut, 'ram_dest', '00')
            self.expect(self.env.dut, 'internal_profile', 0)

    def test_set_profiles(self):
        self._test_uninitialized()

        freq = [i * MHz for i in range(dax.sim.coredevice.urukul.NUM_PROFILES)]
        phase = [0.1 * i for i in range(dax.sim.coredevice.urukul.NUM_PROFILES)]
        amp = [0.1 * i for i in range(dax.sim.coredevice.urukul.NUM_PROFILES)]

        for profile, f, p, a in zip(range(dax.sim.coredevice.urukul.NUM_PROFILES), freq, phase, amp):
            self.env.dut.set(f, phase=p, amplitude=a, profile=profile)
            if profile == dax.sim.coredevice.urukul.DEFAULT_PROFILE:
                self.expect_close(self.env.dut, 'freq', f, places=0)
                self.expect_close(self.env.dut, 'phase', p, places=4)
                self.expect_close(self.env.dut, 'amp', a, places=4)

        for profile, f, p, a in zip(range(dax.sim.coredevice.urukul.NUM_PROFILES), freq, phase, amp):
            self.env.dut.cpld.set_profile(profile)
            self.expect_close(self.env.dut, 'freq', f, places=0)
            self.expect_close(self.env.dut, 'phase', p, places=4)
            self.expect_close(self.env.dut, 'amp', a, places=4)

    def test_set_att(self):
        signal = f'att_{self.env.dut.chip_select - 4}'
        self.expect(self.env.dut.cpld, signal, 'x')
        for _ in range(_NUM_SAMPLES):
            att = self.rng.uniform(0.0, 31.5)
            self.env.dut.set_att(att)
            self.expect(self.env.dut.cpld, signal, att)

    def test_set_att_mu(self):
        signal = f'att_{self.env.dut.chip_select - 4}'
        self.expect(self.env.dut.cpld, signal, 'x')
        self.env.dut.set_att_mu(255)
        self.expect(self.env.dut.cpld, signal, 0 * dB)

    def test_cfg_sw(self):
        ref = '0001000'
        index = self.env.dut.chip_select - 4
        for state in [0, 1]:
            self.env.dut.cfg_sw(state)
            value = ref[index:4 + index] if state else '0000'
            assert value[-1 - index] == str(state)
            self.expect(self.env.dut.cpld, 'sw', value)

    def test_ftw_frequency(self):
        for _ in range(_NUM_SAMPLES):
            f = self.rng.uniform(0 * MHz, 400 * MHz)
            self.assertAlmostEqual(f, self.env.dut.ftw_to_frequency(self.env.dut.frequency_to_ftw(f)), places=0)

    def test_pow_phase(self):
        for _ in range(_NUM_SAMPLES):
            p = self.rng.uniform(0.0, self.env.dut.pow_to_turns(0xFFFF))
            self.assertAlmostEqual(p, self.env.dut.pow_to_turns(self.env.dut.turns_to_pow(p)), places=4)

    def test_asf_amplitude(self):
        for _ in range(_NUM_SAMPLES):
            a = self.rng.uniform(0.0, self.env.dut.asf_to_amplitude(0x3FFF))
            self.assertAlmostEqual(a, self.env.dut.asf_to_amplitude(self.env.dut.amplitude_to_asf(a)), places=4)

    def test_ram_conversion(self):
        self.env.dut.frequency_to_ram([100 * MHz], [0])
        self.env.dut.turns_to_ram([0.1], [0])
        self.env.dut.amplitude_to_ram([0.1], [0])
        self.env.dut.turns_amplitude_to_ram([0.1], [0.5], [0])

    def _create_ram_test_data(self, *, num_steps=100):
        # Frequencies, phases, and amplitudes to convert
        freqs = [(i / num_steps) * MHz for i in range(num_steps)]
        phases_amps = [i / num_steps for i in range(num_steps)]

        # Create arrays for RAM data
        ftw_ram = [0] * num_steps
        asf_ram = [0] * num_steps
        pow_ram = [0] * num_steps
        pow_asf_ram = [0] * num_steps

        # Fill RAM data arrays
        self.env.dut.frequency_to_ram(freqs, ftw_ram)
        self.env.dut.amplitude_to_ram(phases_amps, asf_ram)
        self.env.dut.turns_to_ram(phases_amps, pow_ram)
        self.env.dut.turns_amplitude_to_ram(phases_amps, phases_amps, pow_asf_ram)

        # Return test data
        return {
            RAM_DEST_FTW: ftw_ram,
            RAM_DEST_ASF: asf_ram,
            RAM_DEST_POW: pow_ram,
            RAM_DEST_POWASF: pow_asf_ram,
        }

    def test_ram_mode(self):
        for ram_dest, ram in self._create_ram_test_data().items():
            self.env.dut.cpld.init()
            self.env.dut.init()
            self.env.dut.set_cfr1(ram_enable=0)
            self.env.dut.cpld.io_update.pulse_mu(8)
            self.expect(self.env.dut, 'ram_enable', 0)

            freq = 200 * MHz
            phase = 0.5
            amp = 0.6
            self.env.dut.cpld.set_profile(dax.sim.coredevice.urukul.DEFAULT_PROFILE)
            self.env.dut.set_profile_ram(start=0, end=len(ram))
            self.env.dut.cpld.io_update.pulse_mu(8)
            self.env.dut.write_ram(ram)
            self.env.dut.set_frequency(freq)
            self.env.dut.set_phase(phase)
            self.env.dut.set_amplitude(amp)
            self.env.dut.set_cfr1(ram_enable=1, ram_destination=ram_dest)
            self.expect(self.env.dut, 'ram_enable', 0)
            self.env.dut.cpld.io_update.pulse_mu(8)
            self.expect(self.env.dut, 'ram_enable', 1)
            self.expect(self.env.dut, 'ram_dest', f'{ram_dest:02b}')
            self.expect(self.env.dut, 'phase_mode', 'x')  # Phase mode is not set in pre-ARTIQ 7 RAM programming

            if ram_dest == RAM_DEST_FTW:
                self.expect(self.env.dut, 'freq', 0.0)
            else:
                self.expect_close(self.env.dut, 'freq', freq, places=0)

            if ram_dest in {RAM_DEST_POW, RAM_DEST_POWASF}:
                self.expect(self.env.dut, 'phase', 0.0)
            else:
                self.expect_close(self.env.dut, 'phase', phase, places=4)

            if ram_dest in {RAM_DEST_ASF, RAM_DEST_POWASF}:
                self.expect(self.env.dut, 'amp', 0.0)
            else:
                self.expect_close(self.env.dut, 'amp', amp, places=4)

    if ARTIQ_MAJOR_VERSION >= 7:

        def test_ram_mode_set(self):
            for ram_dest, ram in self._create_ram_test_data().items():
                self.env.dut.cpld.init()
                self.env.dut.init()
                self.env.dut.set_cfr1(ram_enable=0)
                self.env.dut.cpld.io_update.pulse_mu(8)
                self.expect(self.env.dut, 'ram_enable', 0)

                freq = 200 * MHz
                phase = 0.5
                amp = 0.6
                self.env.dut.cpld.set_profile(dax.sim.coredevice.urukul.DEFAULT_PROFILE)
                self.env.dut.set_profile_ram(start=0, end=len(ram))
                self.env.dut.cpld.io_update.pulse_mu(8)
                self.env.dut.write_ram(ram)
                self.env.dut.set(freq, phase=phase, amplitude=amp, ram_destination=ram_dest)
                self.env.dut.set_cfr1(ram_enable=1, ram_destination=ram_dest)
                self.expect(self.env.dut, 'ram_enable', 0)
                self.env.dut.cpld.io_update.pulse_mu(8)
                self.expect(self.env.dut, 'ram_enable', 1)
                self.expect(self.env.dut, 'ram_dest', f'{ram_dest:02b}')
                self.expect(self.env.dut, 'phase_mode', '00')

                if ram_dest == RAM_DEST_FTW:
                    self.expect(self.env.dut, 'freq', 0.0)
                else:
                    self.expect_close(self.env.dut, 'freq', freq, places=0)

                if ram_dest in {RAM_DEST_POW, RAM_DEST_POWASF}:
                    self.expect(self.env.dut, 'phase', 0.0)
                else:
                    self.expect_close(self.env.dut, 'phase', phase, places=4)

                if ram_dest in {RAM_DEST_ASF, RAM_DEST_POWASF}:
                    self.expect(self.env.dut, 'amp', 0.0)
                else:
                    self.expect_close(self.env.dut, 'amp', amp, places=4)


class CompileTestCase(compile_testcase.CoredeviceCompileTestCase):
    DEVICE_CLASS = dax.sim.coredevice.ad9910.AD9910
    DEVICE_KWARGS = {
        'chip_select': 4,
        'cpld_device': 'cpld',
        'pll_en': 0,
    }
    FN_KWARGS = {
        'power_down': {'bits': 0},
        'set_phase_mode': {'phase_mode': 0},
        'write_ram': {'data': [0] * 1024},
        'write64': {'addr': 0x0E, 'data_high': 0, 'data_low': 0},
        'set_mu': {'ftw': 0},
        'set_profile_ram': {'start': 0, 'end': 1023},
        'frequency_to_ftw': {'frequency': 0.0},
        'ftw_to_frequency': {'ftw': 0},
        'turns_to_pow': {'turns': 0.0},
        'pow_to_turns': {'pow_': 0},
        'amplitude_to_asf': {'amplitude': 0.0},
        'asf_to_amplitude': {'asf': 0},
        'frequency_to_ram': {'frequency': [0.0] * 8, 'ram': [0] * 8},
        'turns_to_ram': {'turns': [0.0] * 8, 'ram': [0] * 8},
        'amplitude_to_ram': {'amplitude': [0.0] * 8, 'ram': [0] * 8},
        'turns_amplitude_to_ram': {'turns': [0.0] * 8, 'amplitude': [0.0] * 8, 'ram': [0] * 8},
        'set_ftw': {'ftw': 0},
        'set_pow': {'pow_': 0},
        'set_asf': {'asf': 0},
        'set_frequency': {'frequency': 0.0},
        'set_phase': {'turns': 0.0},
        'set_amplitude': {'amplitude': 0.0},
        'set': {'frequency': 0.0},
        'set_att_mu': {'att': 0},
        'set_att': {'att': 0.0},
        'set_sync': {'in_delay': 0, 'window': 0},
        'cfg_sw': {'state': False},
    }
    FN_EXCLUDE = {'write16', 'write32', 'read16', 'read32', 'read64',
                  'read_ram', 'measure_io_update_alignment'}
    DEVICE_DB = _DEVICE_DB


class SyncDelayCompileTestCase(CompileTestCase):
    DEVICE_KWARGS = {
        "chip_select": 5,
        'cpld_device': 'cpld',
        "pll_en": 0,
        "io_update_delay": 3
    }


class SyncDelayEEPROMCompileTestCase(SyncDelayCompileTestCase):
    DEVICE_KWARGS = {
        "chip_select": 5,
        'cpld_device': 'cpld',
        "pll_en": 0,
        "sync_delay_seed": "eeprom_urukul0:00",
        "io_update_delay": "eeprom_urukul0:00"
    }
