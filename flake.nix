{
  inputs = {
    artiqpkgs.url = git+https://github.com/m-labs/artiq?ref=release-7;
    nixpkgs.follows = "artiqpkgs/nixpkgs";
    sipyco.follows = "artiqpkgs/sipyco";
    flake8-artiq = {
      url = git+https://gitlab.com/duke-artiq/flake8-artiq.git;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    artiq-stubs = {
      url = git+https://gitlab.com/duke-artiq/artiq-stubs.git?ref=release-7;
      inputs.artiqpkgs.follows = "artiqpkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake8-artiq.follows = "flake8-artiq";
    };
    trap-dac-utils = {
      url = git+https://gitlab.com/duke-artiq/trap-dac-utils.git;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, artiqpkgs, sipyco, flake8-artiq, artiq-stubs, trap-dac-utils }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      nistrng = pkgs.python3Packages.callPackage ./nistrng.nix { };

      artiqVersionMajor = builtins.elemAt (builtins.splitVersion artiqpkgs.packages.x86_64-linux.artiq.version) 0;
      daxVersion = "${artiqVersionMajor}.${builtins.toString self.sourceInfo.revCount or 0}+${self.sourceInfo.shortRev or "unknown"}";

      dax = pkgs.python3Packages.callPackage ./derivation.nix {
        inherit daxVersion;
        inherit (artiqpkgs.packages.x86_64-linux) artiq;
        inherit (sipyco.packages.x86_64-linux) sipyco;
        inherit (trap-dac-utils.packages.x86_64-linux) trap-dac-utils;
      };

    in
    rec {
      packages.x86_64-linux = {
        inherit dax;
        inherit (flake8-artiq.packages.x86_64-linux) flake8-artiq;
        inherit (artiq-stubs.packages.x86_64-linux) artiq-stubs;
        inherit (trap-dac-utils.packages.x86_64-linux) trap-dac-utils;
        default = dax;
      };

      devShells.x86_64-linux = {
        default = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              # Packages required for testing
              ps.autopep8
              ps.pytest
              ps.mypy
              ps.pycodestyle
              ps.coverage
              packages.x86_64-linux.flake8-artiq
              packages.x86_64-linux.artiq-stubs
            ] ++ dax.propagatedBuildInputs))
            # Required for compile/hardware testcases
            pkgs.unixtools.ping
            pkgs.lld_11
            pkgs.llvm_11
          ];
        };

        docs = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              # Packages required for documentation
              ps.sphinx
              ps.sphinx_rtd_theme
            ] ++ dax.propagatedBuildInputs))
            pkgs.gnumake
            pkgs.git # Required to set the correct copyright year
          ];
        };

        benches = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              # Packages required for benchmarks
              dax
              nistrng
            ]))
          ];
        };
      };

      # enables use of `nix fmt`
      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
    };

  nixConfig = {
    extra-trusted-public-keys = [ "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc=" ];
    extra-substituters = [ "https://nixbld.m-labs.hk" ];
  };
}
