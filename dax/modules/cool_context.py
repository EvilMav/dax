import typing
import numpy as np

from dax.experiment import *
from dax.interfaces.detection import DetectionInterface
from dax.modules.hist_context import HistogramAnalyzer
from dax.util.ccb import get_ccb_tool

__all__ = ['CoolContextError', 'CoolContext', 'CoolContextCounter']


class CoolContextError(RuntimeError):
    """Class for cool context errors."""
    pass


class CoolContext(DaxModule):
    """Simple context class for managing PMT histogram data from cooling.

    This module can be used as a sub-module of a service providing cooling functions with state monitoring abilities.
    State monitoring during cooling can be useful for example to verify ion presence.
    Compared to :class:`dax.modules.hist_context.HistogramContext`, this module is a lighter and simpler version that
    does not archive data.
    The CoolContext object can directly be passed to the user which can use it as a context
    or call its additional functions.

    Note that the cool context requires a :class:`DetectionInterface` in your system.
    """

    HISTOGRAM_PLOT_KEY_FORMAT: typing.ClassVar[str] = 'plot.{base}.cool_context.histogram'
    """Dataset name for plotting latest histogram."""
    HISTOGRAM_PLOT_NAME: typing.ClassVar[str] = 'histogram'
    """Name of the histogram plot applet."""

    MEAN_COUNT_PLOT_KEY_FORMAT: typing.ClassVar[str] = 'plot.{base}.cool_context.mean_count'
    """Dataset name for plotting latest mean count graph."""
    STDEV_COUNT_PLOT_KEY_FORMAT: typing.ClassVar[str] = 'plot.{base}.cool_context.stdev_count'
    """Dataset name for plotting standard deviation on latest mean count graph."""
    MEAN_COUNT_PLOT_NAME: typing.ClassVar[str] = 'mean count'
    """Name of the mean count plot applet."""

    PLOT_GROUP_FORMAT: typing.ClassVar[str] = '{base}.cool_context'
    """Group to which the plot applets belong."""

    _in_context: np.int32
    _buffer: typing.List[typing.Sequence[int]]
    _first_close: bool
    _plot_base_key: str
    _plot_group_base_key: str
    _histogram_plot_key: str
    _mean_count_plot_key: str
    _stdev_count_plot_key: str
    _plot_group: str

    def build(self, *,  # type: ignore[override]
              plot_base_key: str = 'dax',
              plot_group_base_key: typing.Optional[str] = None
              ) -> None:
        """Build the cool context module.

        The plot base key can be used to group plot datasets and applets as desired.
        The base keys are formatted with the ARTIQ ``scheduler`` object which allows users to
        add experiment-specific information in the base keys.

        By default, plot datasets and applets both are reused. Examples of common settings include:

         - Reuse plot datasets and applets (default)
         - Create unique plot datasets and applets based on the experiment RID:
           ``plot_base_key="{scheduler.rid}"``
         - Create unique plot datasets based on the experiment RID but reuse applets:
           ``plot_base_key="{scheduler.rid}", plot_group_base_key=""``

        :param plot_base_key: Base key for plot dataset keys
        :param plot_group_base_key: Base key for the plot group, same as ``plot_base_key`` if :const:`None`
        """
        assert isinstance(plot_base_key, str), 'Plot base key must be of type str'
        assert isinstance(plot_group_base_key, str) or plot_group_base_key is None, \
            'Plot group base key must be None or of type str'

        # Get CCB tool
        self._ccb = get_ccb_tool(self)
        # Get scheduler
        self._scheduler = self.get_device('scheduler')

        # By default we are not in context
        self._in_context = np.int32(0)
        # The count buffer (buffer appending is a bit faster than dict operations)
        self._buffer = []
        # Flag for the first call to close()
        self._first_close = True

        # Store plot base key
        self._plot_base_key = plot_base_key
        # Store plot group base key
        self._plot_group_base_key = plot_base_key if plot_group_base_key is None else plot_group_base_key

    def init(self) -> None:
        # Generate plot keys
        base: str = self._plot_base_key.format(scheduler=self._scheduler)
        self._histogram_plot_key = self.HISTOGRAM_PLOT_KEY_FORMAT.format(base=base)
        self._mean_count_plot_key = self.MEAN_COUNT_PLOT_KEY_FORMAT.format(base=base)
        self._stdev_count_plot_key = self.STDEV_COUNT_PLOT_KEY_FORMAT.format(base=base)
        # Generate applet plot group
        base = self._plot_group_base_key.format(scheduler=self._scheduler)
        self._plot_group = self.PLOT_GROUP_FORMAT.format(base=base)

    def post_init(self) -> None:
        pass

    """Data handling functions"""

    @portable
    def in_context(self) -> TBool:
        """True if we are in context."""
        return bool(self._in_context)

    @rpc(flags={'async'})
    def append(self, data):  # type: (typing.Sequence[int]) -> None
        """Append PMT data to the histogram (async RPC).

        This function is intended to be fast to allow high input data throughput.
        No type checking is performed on the data.

        :param data: A list of ints representing the PMT counts (i.e. integers) of different ions
        :raises CoolContextError: Raised if called outside the cool context
        """
        if not self._in_context:
            # Called out of context
            raise CoolContextError('append() can only be called inside the cool context')

        # Append the given element to the buffer
        self._buffer.append(data)

    @rpc(flags={'async'})
    def open(self):  # type: () -> None
        """Open the cool context.

        Opening the cool context will clear the buffer and prepare the context for receiving data.

        This function can be used to manually enter the cool context.
        We strongly recommend to use the ``with`` statement instead.

        :raises CoolContextError: Raised if the context was already entered (context is non-reentrant)
        """

        if self._in_context:
            # Prevent context reentry
            raise CoolContextError('The cool context is non-reentrant')

        # Create a new buffer (clearing it might result in data loss due to how the dataset manager works)
        self._buffer = []
        # Increment in context counter
        self._in_context += 1

    @rpc(flags={'async'})
    def close(self):  # type: () -> None
        """Close the cool context.

        This function can be used to manually exit the cool context.
        We strongly recommend to use the ``with`` statement instead.

        :raises CoolContextError: Raised if the cool context was not entered
        """

        if not self._in_context:
            # Called exit out of context
            raise CoolContextError('The exit function can only be called after entering the cool context')

        if self._first_close:
            # Prepare the plot datasets by clearing them
            self.clear_mean_count_plot()
            # Clear flag
            self._first_close = False

        if len(self._buffer):
            # Check consistency of data in the buffer
            if any(len(b) != len(self._buffer[0]) for b in self._buffer):
                raise ValueError('Data in the buffer is ragged')
            if len(self._buffer[0]) == 0:
                raise ValueError('Data elements in the buffer are empty')

            # Transform buffer data to pack counts per ion and convert into histograms
            histograms: typing.Sequence[typing.Counter[int]] = HistogramAnalyzer.raw_to_histograms(self._buffer)

            # Flatten dict-like histograms to uniformly-sized list-style histograms
            max_count: int = max(max(h) for h in histograms)
            flat_histograms = [HistogramAnalyzer.counter_to_ndarray(h, max_count=max_count) for h in histograms]
            # Write result to histogram plotting dataset
            self.set_dataset(self._histogram_plot_key, flat_histograms, broadcast=True, archive=False)

            # Calculate count mean and standard deviation per histogram
            mean_stdev_counts = np.asarray([HistogramAnalyzer.histogram_to_mean_stdev_count(h) for h in histograms],
                                           dtype=np.float64)
            # Transpose data and unpack
            mean_counts, stdev_counts = mean_stdev_counts.transpose()
            # Append results to count mean and standard deviation plotting datasets
            self.append_to_dataset(self._mean_count_plot_key, mean_counts)
            self.append_to_dataset(self._stdev_count_plot_key, stdev_counts)

        # Update context counter
        self._in_context -= 1

    @portable
    def __enter__(self):  # type: () -> None
        """Enter the cool context (see :func:`open`)."""
        self.open()

    @portable  # noqa:ATQ306
    def __exit__(self, exc_type, exc_val, exc_tb):  # type: (typing.Any, typing.Any, typing.Any) -> None  # noqa:ATQ306
        """Exit the cool context (see :func:`close`)."""
        self.close()

    """Applet plotting functions"""

    @rpc(flags={'async'})
    def plot_histogram(self, **kwargs):  # type: (typing.Any) -> None
        """Open the applet that shows a plot of the latest histogram.

        This function can only be called after the module is initialized.

        :param kwargs: Extra keyword arguments for the plot
        """

        # Set default arguments
        kwargs.setdefault('x_label', 'Number of cool counts')
        kwargs.setdefault('y_label', 'Frequency')
        kwargs.setdefault('title', f'RID {self._scheduler.rid}')
        # Plot
        self._ccb.plot_hist_multi(self.HISTOGRAM_PLOT_NAME, self._histogram_plot_key, group=self._plot_group, **kwargs)

    @rpc(flags={'async'})
    def plot_mean_count(self, **kwargs):  # type: (typing.Any) -> None
        """Open the applet that shows a plot of mean count per histogram.

        This function can only be called after the module is initialized.

        :param kwargs: Extra keyword arguments for the plot
        """

        # Set defaults
        kwargs.setdefault('y_label', 'Mean cool counts')
        kwargs.setdefault('title', f'RID {self._scheduler.rid}')
        # Plot
        self._ccb.plot_xy_multi(self.MEAN_COUNT_PLOT_NAME, self._mean_count_plot_key,
                                error=self._stdev_count_plot_key, group=self._plot_group, **kwargs)

    @rpc(flags={'async'})
    def clear_mean_count_plot(self):  # type: () -> None
        """Clear the mean count plot.

        This function can only be called after the module is initialized.
        """
        # Set the mean/stdev count datasets to empty lists
        self.set_dataset(self._mean_count_plot_key, [], broadcast=True, archive=False)
        self.set_dataset(self._stdev_count_plot_key, [], broadcast=True, archive=False)

    @rpc(flags={'async'})
    def disable_histogram_plot(self):  # type: () -> None
        """Close the histogram plot.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet(self.HISTOGRAM_PLOT_NAME, self._plot_group)

    @rpc(flags={'async'})
    def disable_mean_count_plot(self):  # type: () -> None
        """Close the mean count plot.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet(self.MEAN_COUNT_PLOT_NAME, self._plot_group)

    @rpc(flags={'async'})
    def disable_all_plots(self):  # type: () -> None
        """Close all cool context plots.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet_group(self._plot_group)


class CoolContextCounter(CoolContext):
    """Like :class:`CoolContext`, but with counters for the number of times the cool counts fall below a threshold.

    This cool context tracks how many times cool counts fall below a configured threshold on a per-channel basis.
    That information can be used to detect ion loss.
    The counting mechanism used in this context is capable of running in the kernel for fast decision making.

    Note that this context requires a :class:`DetectionInterface` in your system.
    """

    COUNT_THRESHOLD_KEY: typing.ClassVar[str] = 'count_threshold'
    """Key for the default count threshold."""
    DEFAULT_COUNT_THRESHOLD: typing.ClassVar[int] = 2
    """Default count threshold if the system dataset does not exist."""

    _auto_reset: bool
    _cool_count_threshold: np.int32
    _counter: typing.List[np.int32]

    def build(self, *args: typing.Any, auto_reset: bool = True, **kwargs: typing.Any) -> None:
        """Build the cool context counter module.

        :param args: Positional arguments forwarded to :func:`CoolContext.build`
        :param auto_reset: Call :func:`reset_counters` each time :func:`open` is called
        :param args: Keyword arguments forwarded to :func:`CoolContext.build`
        """
        assert isinstance(auto_reset, bool)

        # Call super
        super().build(*args, **kwargs)
        # Store attributes
        self._auto_reset = auto_reset
        self.update_kernel_invariants('_auto_reset')

    def init(self) -> None:
        # Call super
        super().init()
        # Get system datasets
        self._cool_count_threshold = self.get_dataset_sys(self.COUNT_THRESHOLD_KEY, self.DEFAULT_COUNT_THRESHOLD)
        self.update_kernel_invariants('_cool_count_threshold')

    def post_init(self) -> None:
        # Call super
        super().post_init()
        # Allocate memory for counts
        detection = self.registry.find_interface(DetectionInterface)  # type: ignore[misc]
        self._counter = [0] * len(detection.get_pmt_array())

    @rpc(flags={'async'})
    def _super_append(self, data):  # type: (typing.Sequence[int]) -> None
        super().append(data)

    @portable
    def append(self, data: TList(TInt32), cool_count_threshold: TInt32 = -1) -> TNone:  # type: ignore[valid-type]
        """Append PMT data to the histogram.

        This function will offload data to the host using an async RPC call and track if any cool counts are below the
        cool count threshold.

        See also :func:`CoolContext.append`.

        :param data: A list of ints representing the PMT counts (i.e. integers) of different ions
        :param cool_count_threshold: The cool count threshold (any value <0 to use the default cool count threshold)
        :raises CoolContextError: Raised if called outside the cool context
        """
        # Call super
        self._super_append(data)

        # Handle default count threshold
        if cool_count_threshold < 0:
            cool_count_threshold = self._cool_count_threshold
        # Do count thresholding
        for i in range(len(data)):
            if data[i] < cool_count_threshold:
                self._counter[i] += 1

    @rpc(flags={'async'})
    def _super_open(self):  # type: () -> None
        super().open()

    @portable
    def open(self):  # type: () -> None
        # Call super
        self._super_open()
        if self._auto_reset:
            # Reset counters
            self.reset_counters()

    @portable
    def reset_counters(self):  # type: () -> None
        """Reset the cool threshold violation counters.

        This function is called automatically in :func:`open` if auto-reset is enabled.
        """
        for i in range(len(self._counter)):
            self._counter[i] = 0

    @portable
    def get_num_lost_ions(self, counter_threshold: TInt32 = 1) -> TInt32:
        """Get the number of lost ions based on the current counters and the given threshold.

        Detect ion loss by checking how many counters has a value greater or equal to the given threshold.

        This context does not track how many times cooling data was provided. Hence, it does not know what fraction of
        cooling counts did not meet the cool count threshold. Instead, we expect the user to have knowledge of how many
        times cooling data was provided, and how many cool count threshold violations are considered too much.

        This function can be called after :func:`close` and/or after each :func:`append` call.

        :param counter_threshold: The counter threshold
        :return: The number of counters that are >= than the given counter threshold (i.e. number of lost ions)
        """
        # Initialze the number of lost ions to zero
        num_lost_ions = 0

        for c in self._counter:
            if c >= counter_threshold:
                # A counter registered too many cool count threshold violations than allowed, ion was lost
                num_lost_ions += 1

        # Return number of lost ions
        return num_lost_ions
