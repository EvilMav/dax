# mypy: disallow_untyped_defs = False
# mypy: disallow_incomplete_defs = False
# mypy: check_untyped_defs = False

import enum
import typing

from numpy import int32, int64

from artiq.language.core import kernel, portable, rpc, delay_mu
from artiq.language.units import ns
from artiq.language.types import TInt32, TFloat, TStr, TList

from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager


@portable
def _mu_to_voltage(mu: TInt32) -> TFloat:
    voltage = (mu - int32(0x8000)) / (0x8000 / 10.)
    if voltage < -10.0 or voltage >= 10.0:
        raise ValueError("DAC voltage out of bounds")
    return voltage


@rpc
def _to_bool_vec(val: TInt32, width: TInt32) -> TStr:
    return f'{val:0{width}b}'[:width]


@enum.unique
class _CmdAddr(enum.IntEnum):
    """Enum for command addresses."""

    UPDATE = 0x20
    HOLD = 0x21
    CFG = 0x22
    CONTINUOUS = 0x25
    STAGE_CIC = 0x26
    APPLY_CIC = 0x27


class Fastino(DaxSimDevice):

    _NUM_CHANNELS: typing.ClassVar[int] = 32
    """Number of output channels."""
    _NUM_LEDS: typing.ClassVar[int] = 8
    """Number of LEDs."""
    _DAC_MASK: typing.ClassVar[int] = (1 << 16) - 1
    """Mask for a DAC value."""

    channel: int
    width: int
    t_frame: int64
    _dac_reg_mu: typing.List[int]
    _hold_mask: int

    def __init__(self, dmgr: typing.Any, channel: int, log2_width: int = 0, **kwargs: typing.Any):
        # Call super
        super().__init__(dmgr, **kwargs)

        # From ARTIQ code
        self.channel = channel << 8
        self.width = 1 << log2_width
        # frame duration in mu (14 words each 7 clock cycles each 4 ns)
        # self.core.seconds_to_mu(14*7*4*ns)  # unfortunately this may round wrong
        assert self.core.ref_period == 1 * ns
        self.t_frame = int64(14 * 7 * 4)

        # Register signals
        signal_manager = get_signal_manager()
        self._init = signal_manager.register(self, 'init', bool, size=1)
        self._afe_power_down = signal_manager.register(self, 'afe_power_down', bool, size=1)
        self._dac = [signal_manager.register(self, f'v_out_{i}', float) for i in range(self._NUM_CHANNELS)]
        self._continuous = signal_manager.register(self, 'continuous', bool, size=self._NUM_CHANNELS)
        self._cic_config = signal_manager.register(self, 'cic_config', int)
        self._cic = signal_manager.register(self, 'cic', bool, size=self._NUM_CHANNELS)
        self._led = signal_manager.register(self, 'led', bool, size=self._NUM_LEDS)

        # Internal registers
        self._dac_reg_mu = [0] * self._NUM_CHANNELS
        self._hold_mask = 0

    @staticmethod
    def get_rtio_channels(channel, **kwargs):
        return [(channel, None)]

    @kernel
    def init(self):
        self.set_cfg(reset=0, afe_power_down=0, dac_clr=0, clr_err=1)
        delay_mu(self.t_frame)
        self.set_cfg(reset=0, afe_power_down=0, dac_clr=0, clr_err=0)
        delay_mu(self.t_frame)
        self.set_continuous(0)
        delay_mu(self.t_frame)
        self.stage_cic(1)
        delay_mu(self.t_frame)
        self.apply_cic(0xffffffff)
        delay_mu(self.t_frame)
        self.set_leds(0)
        delay_mu(self.t_frame)
        self.set_hold(0)
        delay_mu(self.t_frame)

        # Update signals
        self._init.push(True)

    def _write(self, addr: TInt32, data: TInt32):
        if 0 <= addr < self._NUM_CHANNELS:
            # Update register
            self._dac_reg_mu[addr] = data & self._DAC_MASK
            if not (self._hold_mask & (1 << addr)):
                # Push update to output
                self._dac[addr].push(_mu_to_voltage(self._dac_reg_mu[addr]))

        else:
            # Convert address to a command
            cmd = _CmdAddr(addr)  # Could raise a ValueError

            if cmd == _CmdAddr.UPDATE:
                # Push updates to outputs
                for i in range(self._NUM_CHANNELS):
                    if data & (1 << i):
                        self._dac[i].push(_mu_to_voltage(self._dac_reg_mu[i]))

            elif cmd == _CmdAddr.HOLD:
                # Store hold mask
                self._hold_mask = data & ((1 << self._NUM_CHANNELS) - 1)

            elif cmd == _CmdAddr.CFG:
                # Decompose CFG word
                reset = data & 0x1  # noqa:F841
                afe_power_down = data & 0x2
                dac_clr = data & 0x4
                clr_err = data & 0x8  # noqa:F841

                # Handle AFE power down
                self._afe_power_down.push(bool(afe_power_down))
                # Handle DAC_CLR
                if dac_clr:
                    for d in self._dac:
                        d.push(0.0)

            elif cmd == _CmdAddr.CONTINUOUS:
                # Push continuous signal
                self._continuous.push(_to_bool_vec(data, self._NUM_CHANNELS))

            elif cmd == _CmdAddr.STAGE_CIC:
                # Push CIC config
                self._cic_config.push(data)

            elif cmd == _CmdAddr.APPLY_CIC:
                # Push apply CIC bits
                self._cic.push(_to_bool_vec(data, self._NUM_CHANNELS))

            else:
                raise NotImplementedError(f'Command {cmd!s}')

    @kernel
    def write(self, addr: TInt32, data: TInt32):
        self._write(addr, data)

    @kernel
    def read(self, addr):
        # Not implemented in ARTIQ
        raise NotImplementedError

    @kernel
    def set_dac_mu(self, dac: TInt32, data: TInt32):
        assert self.width == 1, 'log2_width must be 0'
        self.write(dac, data)

    @kernel
    def set_group_mu(self, dac: TInt32, data: TList(TInt32)):  # type: ignore[valid-type]
        assert self.width > 1, 'log2_width must be >0'

        if dac & (self.width - 1):
            raise ValueError("Group index LSBs must be zero")

        # Expanded wide RTIO write
        for i in range(len(data)):
            self._write(dac + (i << 1), data[i] & self._DAC_MASK)
            self._write(dac + (i << 1) + 1, (data[i] >> 16) & self._DAC_MASK)

    @portable
    def voltage_to_mu(self, voltage: TFloat) -> TInt32:
        data = int32(round((0x8000 / 10.) * voltage)) + int32(0x8000)
        if data < 0 or data > 0xffff:
            raise ValueError("DAC voltage out of bounds")
        return data

    @portable
    def voltage_group_to_mu(self, voltage: TList(TFloat), data: TList(TInt32)):  # type: ignore[valid-type]
        for i in range(len(voltage)):
            v = self.voltage_to_mu(voltage[i])
            if i & 1:
                v = data[i // 2] | (v << 16)
            data[i // 2] = int32(v)

    @kernel
    def set_dac(self, dac: TInt32, voltage: TFloat):
        self.set_dac_mu(dac, self.voltage_to_mu(voltage))

    @kernel
    def set_group(self, dac: TInt32, voltage: TList(TFloat)):  # type: ignore[valid-type]
        data = [int32(0)] * (len(voltage) // 2)
        self.voltage_group_to_mu(voltage, data)
        self.set_group_mu(dac, data)

    @kernel
    def update(self, update: TInt32):
        self.write(0x20, update)

    @kernel
    def set_hold(self, hold: TInt32):
        self.write(0x21, hold)

    @kernel
    def set_cfg(self, reset: TInt32 = 0, afe_power_down: TInt32 = 0, dac_clr: TInt32 = 0, clr_err: TInt32 = 0):
        self.write(0x22, (reset << 0) | (afe_power_down << 1)
                   | (dac_clr << 2) | (clr_err << 3))

    @kernel
    def set_leds(self, leds: TInt32):
        self._led.push(_to_bool_vec(leds, self._NUM_LEDS))

    @kernel
    def set_continuous(self, channel_mask: TInt32):
        self.write(0x25, channel_mask)

    @kernel
    def stage_cic_mu(self, rate_mantissa: TInt32, rate_exponent: TInt32, gain_exponent: TInt32):
        if rate_mantissa < 0 or rate_mantissa >= 1 << 6:
            raise ValueError("rate_mantissa out of bounds")
        if rate_exponent < 0 or rate_exponent >= 1 << 4:
            raise ValueError("rate_exponent out of bounds")
        if gain_exponent < 0 or gain_exponent >= 1 << 6:
            raise ValueError("gain_exponent out of bounds")
        config = rate_mantissa | (rate_exponent << 6) | (gain_exponent << 10)
        self.write(0x26, config)

    @kernel
    def stage_cic(self, rate: TInt32) -> TInt32:
        if rate <= 0 or rate > 1 << 16:
            raise ValueError("rate out of bounds")
        rate_mantissa = rate
        rate_exponent = 0
        while rate_mantissa > 1 << 6:
            rate_exponent += 1
            rate_mantissa >>= 1
        order = 3
        gain = 1
        for i in range(order):
            gain *= rate_mantissa
        gain_exponent = 0
        while gain > 1 << gain_exponent:
            gain_exponent += 1
        gain_exponent += order * rate_exponent
        assert gain_exponent <= order * 16
        self.stage_cic_mu(rate_mantissa - 1, rate_exponent, gain_exponent)
        return rate_mantissa << rate_exponent

    @kernel
    def apply_cic(self, channel_mask: TInt32):
        self.write(0x27, channel_mask)
