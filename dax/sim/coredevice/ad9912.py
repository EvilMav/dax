# mypy: disallow_untyped_defs = False
# mypy: disallow_incomplete_defs = False
# mypy: check_untyped_defs = False

import dataclasses

from numpy import int32, int64

from artiq.language.types import TInt32, TInt64, TFloat, TTuple, TBool
from artiq.language.core import kernel, delay, portable
from artiq.language.units import ms, us, ns

from dax.util.artiq_version import ARTIQ_MAJOR_VERSION
from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager
from dax.sim.coredevice.urukul import CPLD


@portable(flags={"fast-math"})
def _pow_to_turns(pow_: TInt32) -> TFloat:
    return float(pow_ / (1 << 14))


@dataclasses.dataclass
class _AD9912Registers:
    freq: float = 0.0
    phase: float = 0.0

    def write(self, freq, phase) -> None:
        self.freq = freq
        self.phase = phase

    def read(self) -> TTuple([TFloat, TFloat]):  # type: ignore[valid-type]
        return self.freq, self.phase


class AD9912(DaxSimDevice):

    cpld: CPLD
    ftw_per_hz: float
    _reg: _AD9912Registers

    def __init__(self, dmgr, chip_select, cpld_device, sw_device=None, pll_n=10, **kwargs):
        # Call super
        super(AD9912, self).__init__(dmgr, **kwargs)

        # From ARTIQ code
        self.cpld = dmgr.get(cpld_device)
        pass
        self.bus = self.cpld.bus
        assert 4 <= chip_select <= 7
        self.chip_select = chip_select
        if sw_device:
            self.sw = dmgr.get(sw_device)
            pass
        self.pll_n = pll_n
        sysclk = self.cpld.refclk / [1, 1, 2, 4][self.cpld.clk_div] * pll_n
        assert sysclk <= 1e9
        self.ftw_per_hz = 1 / sysclk * (int64(1) << 48)

        # Register signals
        signal_manager = get_signal_manager()
        self._init = signal_manager.register(self, 'init', bool, size=1)
        self._freq = signal_manager.register(self, 'freq', float)
        self._phase = signal_manager.register(self, 'phase', float)

        # Internal registers
        self._reg = _AD9912Registers()

        # Register to IO updates
        self.cpld.io_update.pulse_subscribe(self._io_update)

    def _io_update(self):
        self._freq.push(self._reg.freq)
        self._phase.push(self._reg.phase)

    @kernel
    def write(self, addr: TInt32, data: TInt32, length: TInt32):
        raise NotImplementedError

    @kernel
    def read(self, addr: TInt32, length: TInt32) -> TInt32:
        raise NotImplementedError

    @kernel
    def init(self):
        # Delays and some IO updates from ARTIQ code
        self.cpld.io_update.pulse(2 * us)
        delay(50 * us)
        self.cpld.io_update.pulse(2 * us)
        delay(1 * ms)

        # Set init signal
        self._init.push(True)

    @kernel
    def set_att_mu(self, att: TInt32):
        self.cpld.set_att_mu(self.chip_select - 4, att)

    @kernel
    def set_att(self, att: TFloat):
        self.cpld.set_att(self.chip_select - 4, att)

    def _set_mu(self, ftw: TInt64, pow_: TInt32):
        freq = self.ftw_to_frequency(int64(ftw) & 0xFFFFFFFFFFFF)
        phase = _pow_to_turns(int32(pow_) & 0x3FFF)
        self._reg.write(freq, phase)
        self.cpld.io_update.pulse(10 * ns)

    if ARTIQ_MAJOR_VERSION < 7:  # pragma: no cover
        # noinspection PyShadowingBuiltins
        @kernel
        def set_mu(self, ftw: TInt64, pow: TInt32):
            self._set_mu(ftw, pow_=pow)
    else:
        @kernel
        def set_mu(self, ftw: TInt64, pow_: TInt32):
            self._set_mu(ftw, pow_=pow_)

    @portable(flags={"fast-math"})
    def frequency_to_ftw(self, frequency: TFloat) -> TInt64:
        return int64(round(float(self.ftw_per_hz * frequency))) & ((int64(1) << 48) - 1)

    @portable(flags={"fast-math"})
    def ftw_to_frequency(self, ftw: TInt64) -> TFloat:
        return float(ftw / self.ftw_per_hz)

    @portable(flags={"fast-math"})
    def turns_to_pow(self, phase: TFloat) -> TInt32:
        return int32(round(float((1 << 14) * phase))) & int32(0xffff)

    @kernel
    def set(self, frequency: TFloat, phase: TFloat = 0.0):
        self.set_mu(self.frequency_to_ftw(frequency),
                    self.turns_to_pow(phase))

    @kernel
    def cfg_sw(self, state: TBool):
        self.cpld.cfg_sw(self.chip_select - 4, state)

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def get_att_mu(self) -> TInt32:
            return self.cpld.get_channel_att_mu(self.chip_select - 4)

        @kernel
        def get_att(self) -> TFloat:
            return self.cpld.get_channel_att(self.chip_select - 4)

        @kernel
        def get_mu(self) -> TTuple([TInt64, TInt32]):  # type: ignore[valid-type]
            freq, phase = self.get()
            return self.frequency_to_ftw(freq), self.turns_to_pow(phase)

        @portable(flags={"fast-math"})
        def pow_to_turns(self, pow_: TInt32) -> TFloat:
            return _pow_to_turns(pow_)

        @kernel
        def get(self) -> TTuple([TFloat, TFloat]):  # type: ignore[valid-type]
            return self._reg.read()
