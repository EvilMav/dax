import typing

from numpy import int32, int64

from artiq.language.core import portable
from artiq.language.types import TNone, TInt32, TInt64, TFloat

__all__ = ["PCG_XSH_RR"]


class PCG_XSH_RR:
    """Pseudorandom number generator (PRNG) based on a permuted congruential generator (PCG).

    This PCG PRNG implmentation is based on https://www.pcg-random.org/.

    This class implements the PCG-XSH-RR-64-32 variation, which means it is based on xorshift operations and random
    rotation with a 64-bit state and 32-bit output.

    Note that the operations in this class rely on integer overflow behavior that can cause NumPy warnings when executed
    on the host. To suppress warnings, use a context as shown below:

        with np.testing.suppress_warnings() as sup:
            sup.filter(RuntimeWarning, "overflow encountered in .*")
            ...
    """

    DEFAULT_INCREMENT: typing.ClassVar[int64] = int64(1442695040888963407)
    """The default increment for a 64-bit state. Normally uint64, but fits in int64 too."""
    _MULTIPLIER: typing.ClassVar[int64] = int64(6364136223846793005)
    """The carefully chosen multiplier for a 64-bit state. Normally uint64, but fits in int64 too."""
    _RANDFLOAT_DIV: typing.ClassVar[float] = float(1 << 64)
    """Precomputed :func:`randfloat` divider, required to satisfy the ARTIQ compiler."""

    _state: int64
    _increment: int64

    kernel_invariants: typing.ClassVar[typing.Set[str]] = {"_MULTIPLIER", "_RANDFLOAT_DIV", "core", "_increment"}

    def __init__(self, core: typing.Any, *, increment: typing.Union[int, int64] = DEFAULT_INCREMENT) -> None:
        """Create a PRNG.

        **Note: this PRNG must be initialized after creation for proper opration.**

        :param core: Reference to the ARTIQ core object
        :param increment: A 64-bit increment value (will be made odd in case it is not)
        """
        self.core = core
        self._state = int64(0)
        self._increment = int64(increment | 1)

    @portable
    def _step_r(self) -> TNone:
        """Advance the internal state of the PRNG."""
        self._state = int64(int64(self._state * self._MULTIPLIER) + self._increment)

    @portable
    def init(self, seed: TInt64) -> TNone:
        """Initialize the internal state of the PRNG.

        Suggestions for arbitrary seed sources include ``now_mu()`` (kernel) and ``time.time_ns()`` (host).

        :param seed: A 64-bit seed to initialize the PRNG with
        """
        self._state = int64(0)
        self._step_r()
        self._state = int64(self._state + int64(seed))
        self._step_r()

    @portable
    def randint32(self) -> TInt32:
        """Generate a random 32-bit integer."""
        # Store old state
        oldstate = self._state
        # Advance state
        self._step_r()
        # Apply output function on old state
        lower = int64((oldstate >> 18) & 0x3FFFFFFFFFFF)  # Drop sign bits from arithmetic shift
        xorshifted = int64(((lower ^ oldstate) >> 27) & 0xFFFFFFFF)  # u64 solves sign bit issue with arithmetic shift
        rot = int32(int32(oldstate >> 59) & 0x1F)  # Drop sign bits from arithmetic shift
        return int32(int32(xorshifted >> rot) | int32(xorshifted << int32((-rot) & 31)))

    @portable
    def randint64(self) -> TInt64:
        """Generate a random 64-bit integer."""
        lower = int64(int64(self.randint32()) & 0xFFFFFFFF)
        upper = int64(int64(self.randint32()) & 0xFFFFFFFF)
        return int64(lower | int64(upper << 32))

    @portable
    def randrange(self, start: TInt32, stop: TInt32) -> TInt32:
        """Generate a random 32-bit integer N such that ``start <= N < stop``.

        This function is unbiased based on OpenBSD's method, see https://www.pcg-random.org/posts/bounded-rands.html.
        """
        span = int64(max(int64(1), int64(stop) - int64(start)))
        threshold = int64((-span & 0xFFFFFFFF) % span)  # Mask -span to mimic unary minus behavior of u32
        result = int32(0)  # This variable is here to make the ARTIQ compiler happy

        while True:
            r = int64(self.randint32()) & 0xFFFFFFFF  # Use i64 to interpret i32 as u32
            if r >= threshold:
                result = int32(int32(r % span) + start)
                break  # This control flow is here to make the ARTIQ compiler happy

        return result  # This control flow is here to make the ARTIQ compiler happy

    @portable
    def randfloat(self) -> TFloat:
        """Generate a random float in the range 0.0 <= X < 1.0.

        Note that a random float from this PRNG is based on 64-bit randomness.
        """
        return abs(float(self.randint64()) / self._RANDFLOAT_DIV)
